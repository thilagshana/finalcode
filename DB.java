package bcas.Ap.book.store;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class DB {
	public static Connection getConnection() {
		Connection con = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/booklvs", "root", "root");
		} catch (Exception e) {
			System.out.println(e);
		}
		return con;
	}

	public static int Usersave(String name, String password, String address, String city, int phoneNum) {
		int status = 0;
		try {
			Connection con = DB.getConnection();
			PreparedStatement ps = con
					.prepareStatement("insert into user(name,password,address,city,contact) values(?,?,?,?,?)");
			ps.setString(1, name);
			ps.setString(2, password);
			ps.setString(3, address);
			ps.setString(4, city);
			ps.setInt(5, phoneNum);
			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public static boolean LoginValidate(String name, String password) {
		boolean status = false;
		try {
			Connection con = DB.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from user where name=? and password=?");
			ps.setString(1, name);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			status = rs.next();
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public static boolean bookValidate(String book) {
		boolean status = false;
		try {
			Connection con = DB.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from book where title=?");
			ps.setString(1, book);

			ResultSet rs = ps.executeQuery();
			status = rs.next();
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public static void bookView(String book) {

		try {
			Connection con = DB.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from book where title=?");
			ps.setString(1, book);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				// Retrieve by column name
				int id = rs.getInt("id");
				String isbn = rs.getString("ISBN");
				String title = rs.getString("title");
				String author = rs.getString("author");
				String publisher = rs.getString("publisher");
				int quantity = rs.getInt("quantity");
				double price = rs.getDouble("price");
				// Display values
				System.out.print("ID: " + id);
				System.out.print(", ISBN: " + isbn);
				System.out.println(", Book Title: " + title);
				System.out.println(", Author: " + author);
				System.out.println(", Publisher: " + publisher);
				System.out.println(", Quantity: " + quantity);
				System.out.println(", Price: " + price);
			}

			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}

	}

}
