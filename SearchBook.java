package bcas.Ap.book.store;

import java.util.Scanner;

public class SearchBook {
	Scanner sc = new Scanner(System.in);
	static String book;

	public static void SearchView() {
		Scanner sc = new Scanner(System.in);

		System.out.println("Search book:- ");
		book = sc.nextLine();
		if (DB.bookValidate(book)) {
			DB.bookView(book);
		} else {
			System.out.println("Sorry your search book not exist! Search again");
		}
	}
}
